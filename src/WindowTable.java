import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


class Entry { //used for iterating through the data send by the server
	String name;
	String place;
	Calendar c;

	Entry(String name,String place, Calendar c ) {
		this.name = name;
		this.place = place;
		this.c = c;
	}
}

public class WindowTable {

	public static class Table {

		//name, Datum, anmeldung, abmeldung, zeit
		private final SimpleStringProperty p0,p1,p2,p3,p4;

		Table(String p1, String p2, String p3, String p4, String p5) {
			this.p0 = new SimpleStringProperty(p1);
			this.p1 = new SimpleStringProperty(p2);
			this.p2 = new SimpleStringProperty(p3);
			this.p3 = new SimpleStringProperty(p4);
			this.p4 = new SimpleStringProperty(p5);
		}

		public String getP0() {return p0.get();}
		public void setP0(String v) {p0.set(v);}
		public String getP1() {return p1.get();}
		public void setP1(String v) {p1.set(v);}
		public String getP2() {return p2.get();}
		public void setP2(String v) {p2.set(v);}
		public String getP3() {return p3.get();}
		public void setP3(String v) {p3.set(v);}
		public String getP4() {return p4.get();}
		public void setP4(String v) {p4.set(v);}
	}

	private TableView<Table> table; //TODO write to table
	private TableColumn<Table, String> idColumn, dateColumn,anColumn,abColumn,zeitColumn;
	private boolean errorOccurred = false;
	
	private static boolean sameDay(Calendar c1, Calendar c2) {
		return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
		       c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
	}

	private void addFields(ArrayList<ArrayList<String>> fields, Entry start_entry, Entry last_entry) {
		//date formatter
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		String formattedDate = formatter.format(start_entry.c.getTime());
		formatter = new SimpleDateFormat("HH:mm");
		String formattedStartTime = formatter.format(start_entry.c.getTime());
		String formattedEndTime = null;
		if(last_entry != null)
			formattedEndTime = formatter.format(last_entry.c.getTime());

		fields.add(new ArrayList<>());
		fields.get(fields.size()-1).add(start_entry.name); //name
		fields.get(fields.size()-1).add(formattedDate); //date
		fields.get(fields.size()-1).add(formattedStartTime); //starttime
		if(last_entry != null)
			fields.get(fields.size()-1).add(formattedEndTime); //endtime
		else
			fields.get(fields.size()-1).add(""); //empty endtime

		if(last_entry != null) {
			long minutes = ChronoUnit.MINUTES.between(start_entry.c.toInstant(), last_entry.c.toInstant());
			String formattedDuration = String.format("%2dh %2dmin", minutes / 60, minutes % 60);
			fields.get(fields.size()-1).add(formattedDuration); //duration
		} else {
			fields.get(fields.size()-1).add("Nicht abgemeldet"); //error if only 1 login
		}
	}

	WindowTable(String input) {
		String[] lines = input.split("\n"); //split by newline
		ArrayList<Entry> entries = new ArrayList<>();
		for (String line : lines) {
			String[] fields = line.split("\\|"); //split by |, escape because of regex
			if (fields.length != 3) {
				errorOccurred = true;
				//TODO: error
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); //time format from the server
				Calendar c = Calendar.getInstance();
				Date d = null;
				try {
					d = sdf.parse(fields[2]);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				c.setTime(Objects.requireNonNull(d));
				entries.add(new Entry(fields[0], fields[1], c)); //create entry
			}
		}
		
		Entry last_entry = null; //if null or not on the same day, start a new day
		Entry start_entry = null; //the start of the current day
		boolean first_run = true;
		ArrayList<ArrayList<String>> fields = new ArrayList<>();
		
		for(Entry current : entries) {
			if(first_run) {
				start_entry = current;
				first_run = false;
				continue;
			}
			//if time is not the same day, finish current day, remember start time and continue with loop
			if(!sameDay(current.c, start_entry.c))
			{
				//last entry might be null if the entry following the start entry is already the next day, in that case the user didn't log out
				addFields(fields, start_entry, last_entry);
				last_entry = null;
				start_entry = current;
			} else {
				last_entry = current;
			}
		}

		addFields(fields, Objects.requireNonNull(start_entry), last_entry);

		buildWindow(fields);
	}
	
	private void buildWindow(ArrayList<ArrayList<String>> grid) {
		final ObservableList<Table> data = FXCollections.observableArrayList();
		for (ArrayList<String> row:grid) {
			data.add(new Table(row.get(0), row.get(1), row.get(2),row.get(3), row.get(4)));
		}

		table = new TableView<>();

		idColumn = new TableColumn<>("Name");
		idColumn.setCellValueFactory(new PropertyValueFactory<>("p0"));
		idColumn.setPrefWidth(100);

		dateColumn = new TableColumn<>("Datum");
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("p1"));
		dateColumn.setPrefWidth(100);

		anColumn = new TableColumn<>("Anmeldung");
		anColumn.setCellValueFactory(new PropertyValueFactory<>("p2"));
		anColumn.setPrefWidth(100);

		abColumn = new TableColumn<>("Abmeldung");
		abColumn.setCellValueFactory(new PropertyValueFactory<>("p3"));
		abColumn.setPrefWidth(100);

		zeitColumn = new TableColumn<>("Zeit");
		zeitColumn.setCellValueFactory(new PropertyValueFactory<>("p4"));
		zeitColumn.setPrefWidth(125);

		table.getColumns().addAll(idColumn, dateColumn,anColumn, abColumn,zeitColumn);
		table.setItems(data);

		StackPane secondaryLayout = new StackPane();
		secondaryLayout.getChildren().add(table);

		Scene secondScene = new Scene(secondaryLayout, 550, 600);

		// New window (Stage)
		Stage newWindow = new Stage();
		newWindow.setTitle("Benutzer");
		newWindow.setScene(secondScene);

		newWindow.show();

		if(errorOccurred) {
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.setTitle("Korrupte Daten");
			alert.setHeaderText("Unvollständige or korrupte Daten vom Server empfangen");
			alert.setContentText("Manche Daten vom Server scheinen beschädigt gewesen zu sein.");
			alert.showAndWait();
		}
	}
}

