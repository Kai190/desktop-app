import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class Window extends Application {

	//LocalDate now = LocalDate.now();
	private TabPane tabs;
	//private Client client;
	private RadioButton choicepw, choicepin;
	private CheckBox checkDatum, checkinkl;
	private DatePicker dpbegin,dpend, dptaggrp;
	private Image imgIcon = new Image("file:resources/index.png");
	private final byte VERSION = 1;

	private TextField inputUsername,inputPassword, userrequested;
	private PasswordField oldpin, newinput, repeat_newinput;

	@Override
	public void start(Stage primaryStage) throws Exception {
		//new FXMLLoader(getClass().getResource("loginwindow.fxml"));
		Parent root = FXMLLoader.load(new URL(new URL("file:"), "./resources/loginwindow.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setMinHeight(600);
		primaryStage.setMinWidth(400);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Menu");
		primaryStage.show();

		tabs = (TabPane)scene.lookup("#tabs");

		choicepw = (RadioButton)scene.lookup("#choicepw");
		choicepw.setOnMouseClicked(e-> oldpin.setDisable(true));
		choicepin = (RadioButton)scene.lookup("#choicepin");
		choicepin.setOnMouseClicked(e-> oldpin.setDisable(false));
		
		checkDatum = (CheckBox)scene.lookup("#checkDatum");

		checkDatum.setOnAction(e-> {
			if(checkDatum.isSelected())
			{
				dpbegin.setDisable(false);
				dpend.setDisable(false);
			}
			else
			{
				dpbegin.setDisable(true);
				dpend.setDisable(true);
			}
		});

		checkinkl = (CheckBox)scene.lookup("#checkinkl");

		ImageView icon = (ImageView) scene.lookup("#icon");
		icon.setImage(imgIcon);

		inputUsername = (TextField)scene.lookup("#inputUsername");
		inputPassword = (TextField)scene.lookup("#inputPassword");
		userrequested = (TextField)scene.lookup("#userrequested");
		dpbegin = (DatePicker)scene.lookup("#dpbegin");
		dpend = (DatePicker)scene.lookup("#dpend");
		oldpin = (PasswordField)scene.lookup("#oldpin");
		newinput = (PasswordField)scene.lookup("#newinput");
		repeat_newinput = (PasswordField)scene.lookup("#repeat_newinput");
		dptaggrp = (DatePicker)scene.lookup("#dptaggrp");

		Button cmdSend = (Button) scene.lookup("#cmdSend");
		Button cmdClose = (Button) scene.lookup("#cmdClose");

		cmdSend.setOnMouseClicked(e -> startCommunication());
		cmdClose.setOnMouseClicked(e -> System.exit(0));
	}

	private Date incrementDay(DatePicker convertday) {

		LocalDate ld = convertday.getValue();
		Calendar cal =  Calendar.getInstance();
		cal.set(ld.getYear(), ld.getMonthValue() - 1, ld.getDayOfMonth());

		cal.setTimeInMillis(cal.getTimeInMillis() + 86400000);

		return cal.getTime();

	}
	private void startCommunication() {
		Client client;

		int mode = 0;

		if(tabs.getSelectionModel().isSelected(0) && checkDatum.isSelected()) {
			mode = 1;
		}
		else if(tabs.getSelectionModel().isSelected(1) && choicepw.isSelected()) {
			mode = 2;
		}
		else if(tabs.getSelectionModel().isSelected(1) && choicepin.isSelected()) {
			mode = 3;
		}
		else if(tabs.getSelectionModel().isSelected(2) ) {
			mode = 4;
		}
		if(Ueberpruefung(mode)) {
			client = new Client("127.0.0.1",6458);

			if (client.getStatus() != VERSION) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Versionen falsch");
				alert.showAndWait();
				return;
			}

			String[] modexp = null;
			try {
				modexp = new String[]{client.readToNull(),client.readToNull()};
			} catch (IOException e1){
				e1.printStackTrace();
			}
			BigInteger mod = new BigInteger(modexp[0]);
			BigInteger exp = new BigInteger(modexp[1]);

			Crypter.buildKeyPair(mod, exp);

			String usersend = Crypter.encrypt(inputUsername.getText()) +
					"\0" +
					Crypter.encrypt(inputPassword.getText()) +
					"\0" +
					Crypter.getMod().toString() +
					"\0" +
					Crypter.getExp().toString() +
					"\0";
			client.writeMessage(usersend);

			if (client.getStatus() != 0) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Anmeldung am Server nicht möglich!");
				alert.showAndWait();
				return;
			}
			try {
				client.getOutputStream().write((byte)mode);
				client.getOutputStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}

			String message = Crypter.encrypt(userrequested.getText()) + "\0";
			if (mode==0 || mode == 1 || mode == 2 || mode == 3){

				if (mode == 1 || mode == 0 ){
					message += Crypter.encrypt(dpbegin.getValue().toString()) + "\0" + Crypter.encrypt("" + new SimpleDateFormat("yyyy-MM-dd").format(incrementDay(dpend))) + "\0";
				}
				else if (mode == 2){
					message += Crypter.encrypt(newinput.getText()) + "\0";
				}
				else {
					message += Crypter.encrypt(oldpin.getText()) + "\0" + Crypter.encrypt(newinput.getText()) + "\0";
				}
				client.writeMessage(message);
			}
			else {
				message += Crypter.encrypt(dptaggrp.getValue().toString()) + "\0";
				client.writeMessage(message);
				try {
					client.getOutputStream().write(checkinkl.isSelected()?(byte)1:(byte)0);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				byte status = client.getStatus();
				if(!statusUeberpruefen(status))
					return;
				ArrayList<String> parts = new ArrayList<>() ;
				try {
					while (client.getStatus() == 1) {
						parts.add(client.readToNull());
					}
					parts.add(client.readToNull());
				} catch (IOException e1){
					e1.printStackTrace();
				}
				StringBuilder text = new StringBuilder();
				for (String part : parts) {
					String tmp = Crypter.decrypt(part);
					if (tmp.equals("ERROR") && mode <= 1) {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Error");
						alert.setHeaderText("Keine Daten im eingegebenen Bereich vorhanden!");
						alert.showAndWait();
						return;
					}
					text.append(tmp);
				}
				client.close();
				if(mode > 1) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Achtung");
					if(mode==2){
						alert.setHeaderText("Passwort wurde erfolgreich geändert");
					}
					else if(mode==3){
						alert.setHeaderText("PIN wurde erfolgreich geändert");
					}

					alert.showAndWait();
				}
				if(mode <= 1)
					new WindowTable(text.toString());
				if(mode == 4)
					new WindowTableGrp(text.toString());
			} catch(NullPointerException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information");
				alert.setHeaderText("Keine Informationen gefunden!");
				alert.setContentText("Bitte überprüfen Sie Ihre Eingabe!");
				alert.showAndWait();
			}
		}
	}

	private boolean statusUeberpruefen(byte status) {
		System.out.println("status: " + status);

		if(status != 0){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Achtung");
			alert.setHeaderText("Fehler!");
			switch (status){
				case 1:
					alert.setContentText("Login fehlgeschlagen!");
					break;
				case 2:
					alert.setContentText("Keine Zugriffsrechte!");
					break;
				case 3:
					alert.setContentText("ungültiger Benutzer!");
					break;
				case 4:
					alert.setContentText("Ungültiger PIN!");
					break;
				case 5:
					alert.setContentText("Datenbankfehler! Bitte informieren Sie den Admin!");
					break;
				case 6:
					alert.setContentText("Verschlüsselungsfehler! Bitte informieren Sie den Admin!");
					break;
				case 7:
					alert.setContentText("Kommunikation mit dem Server fehlgeschlagen! Bitte informieren Sie den Admin!");
					break;
				case 8:
					alert.setContentText("Fehlercode 8!");
					break;
				case 9:
					alert.setContentText("Fehlercode 9");
					break;
				case 10:
					alert.setContentText("Fehlercode 10");
					break;
				default:
					alert.setContentText("Unbekannter Fehler! Bitte informieren Sie den Admin!");
			}
			alert.showAndWait();
			return false;
		}
		return true;
	}

	private boolean Ueberpruefung(int mode) {

		if (dpbegin.getValue() == null && mode == 1 || mode == 0)
			dpbegin.setValue(LocalDate.of(1970, 1, 1));

		if (dpend.getValue() == null && mode == 1 || mode == 0)
			dpend.setValue(LocalDate.now());

		if (dptaggrp.getValue() == null && mode == 4)
			dptaggrp.setValue(LocalDate.now());

		if(userrequested.getText().isEmpty())
			userrequested.setText(inputUsername.getText());

		if (!newinput.getText().equals(repeat_newinput.getText()) && mode >= 2) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Passwörter stimmen nicht überein");
			alert.setContentText("Bitte überprüfen Sie Ihre Eingabe!");
			alert.showAndWait();
			return false;
		}
		return true;
	}

}
