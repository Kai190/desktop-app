import java.io.*;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.net.InetSocketAddress;
import java.net.Socket;

class Client {
	private Socket socket;
	private BufferedReader bufferedReader;

	Client(String ip, int port){
		connectTo(ip,port);
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void connectTo(String ip, int port) {
	 	try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(ip,port), 2000);// verbindet sich mit Server
		} catch (IOException e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not connected to "+ip);
			alert.setContentText(e.getMessage());
			alert.showAndWait();
			return;
		}
	}
	void close()
	{
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void writeMessage(String nachricht) {
	 	try {
			PrintWriter printWriter =
	 			new PrintWriter(
	 	 		new OutputStreamWriter(
	 			socket.getOutputStream()
	 	 		));
	 		printWriter.print(nachricht);
	 		printWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	OutputStream getOutputStream() {
		try {
			return socket.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	byte getStatus() {
		try {
			return (byte) bufferedReader.read();
		} catch (IOException e) {
			e.printStackTrace();
			return -2;
		}
	}

	String readToNull() throws IOException {
		StringBuilder s = new StringBuilder();
		int x = bufferedReader.read();
		if (x >= 0) {
			char c = (char) x;
			while (c != '\0') {
				s.append(c);
				x = bufferedReader.read();
				if (!(x >= 0)) {
					break;
				}
				c = (char) x;
			}
		}
		return s.toString();
	}

}
