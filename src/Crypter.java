import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

class Crypter {
	private static PrivateKey readPrivateKey;
	private static PublicKey writePubKey;
	private static BigInteger exp,mod;

	static BigInteger getExp() {return Crypter.exp;}
	static BigInteger getMod() {return Crypter.mod;}

	private static void buildReadKey() {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");

			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(1024);
			KeyPair readKeyPair = keyPairGenerator.genKeyPair();

			readPrivateKey = readKeyPair.getPrivate();
			RSAPublicKeySpec writeSpecs = factory.getKeySpec(readKeyPair.getPublic(),RSAPublicKeySpec.class);
			Crypter.mod = writeSpecs.getModulus();
		   	Crypter.exp = writeSpecs.getPublicExponent();

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
	private static void buildWriteKey(BigInteger mod, BigInteger exp) {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");

			RSAPublicKeySpec readSpec=new RSAPublicKeySpec(mod, exp);
			writePubKey = factory.generatePublic(readSpec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
	static void buildKeyPair(BigInteger mod, BigInteger exp) {
		buildReadKey();
		buildWriteKey(mod,exp);
	}

	static String encrypt(String message){
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, writePubKey);

			return new String (Base64.getEncoder().encode(cipher.doFinal(message.getBytes())), StandardCharsets.UTF_8);
		}catch(NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
		//System.out.println(e);
			return "ERROR";
		}
	}

	static String decrypt(String encrypted) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
			cipher.init(Cipher.DECRYPT_MODE, readPrivateKey);

			return new String (cipher.doFinal(Base64.getDecoder().decode(encrypted)), StandardCharsets.UTF_8);
		}catch(NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchPaddingException e) {
			//System.out.println(e);
			return "ERROR";
		}
	}

}
