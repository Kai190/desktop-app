import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


class EntryGrp { //used for iterating through the data send by the server
	String name;
	String group;
	Calendar c;

	EntryGrp(String name,String group, Calendar c ) {
		this.name = name;
		this.group = group;
		this.c = c;
	}
}

public class WindowTableGrp {

	public static class Table {

		//gruppe, name, Datum, anwesenheit
		private final SimpleStringProperty p0,p1,p2,p3;

		Table(String p1, String p2, String p3, String p4) {
			this.p0 = new SimpleStringProperty(p1);
			this.p1 = new SimpleStringProperty(p2);
			this.p2 = new SimpleStringProperty(p3);
			this.p3 = new SimpleStringProperty(p4);
		}

		public String getP0() {return p0.get();}
		public void setP0(String v) {p0.set(v);}
		public String getP1() {return p1.get();}
		public void setP1(String v) {p1.set(v);}
		public String getP2() {return p2.get();}
		public void setP2(String v) {p2.set(v);}
		public String getP3() {return p3.get();}
		public void setP3(String v) {p3.set(v);}
	}

	private TableView<Table> table; //TODO write to table
	private TableColumn<Table, String> groupColumn, idColumn, dateColumn,presenceColumn;
	private boolean errorOccurred = false;
	
	private static boolean sameDay(Calendar c1, Calendar c2) {
		return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
		       c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
	}

	private void addFields(ArrayList<ArrayList<String>> fields, EntryGrp start_entry) {

		//date formatter
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		String formattedDate = formatter.format(start_entry.c.getTime());
		fields.add(new ArrayList<>());
		fields.get(fields.size() - 1).add(start_entry.group); //gruppe
		fields.get(fields.size() - 1).add(start_entry.name); //name
		fields.get(fields.size() - 1).add(formattedDate); //date
		if(start_entry!=null) {
			fields.get(fields.size() - 1).add("anwesend");
		}else {
			fields.get(fields.size() - 1).add("nicht anwesend");
		}

	}

	WindowTableGrp(String input) {
		String[] lines = input.split("\n"); //split by newline
		ArrayList<EntryGrp> entries = new ArrayList<>();
		for (String line : lines) {
			String[] fields = line.split("\\|"); //split by |, escape because of regex
			if (fields.length != 3) {
				errorOccurred = true;
				//TODO: error
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); //time format from the server
				Calendar c = Calendar.getInstance();
				Date d = null;
				try {
					d = sdf.parse(fields[2]);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				c.setTime(Objects.requireNonNull(d));
				entries.add(new EntryGrp(fields[0], fields[1], c)); //create entry
			}
		}

		EntryGrp start_entry = null; //the start of the current day
		boolean first_run = true;
		ArrayList<ArrayList<String>> fields = new ArrayList<>();
		
		for(EntryGrp current : entries) {
			if(first_run) {
				start_entry = current;
				first_run = false;
				continue;
			}
			//if time is not the same day, finish current day, remember start time and continue with loop
			if(!sameDay(current.c, start_entry.c))
			{
				//last entry might be null if the entry following the start entry is already the next day, in that case the user didn't log out
				addFields(fields, start_entry);
				start_entry = current;
			}
		}

		addFields(fields, start_entry);

		buildWindow(fields);
	}
	
	private void buildWindow(ArrayList<ArrayList<String>> grid) {
		final ObservableList<Table> data = FXCollections.observableArrayList();
		for (ArrayList<String> row:grid) {
			data.add(new Table(row.get(0), row.get(1), row.get(2),row.get(3)));
		}

		table = new TableView<>();

		groupColumn = new TableColumn<>("Gruppe");
		groupColumn.setCellValueFactory((new PropertyValueFactory<>("p0")));

		idColumn = new TableColumn<>("Name");
		idColumn.setCellValueFactory(new PropertyValueFactory<>("p1"));
		idColumn.setPrefWidth(100);

		dateColumn = new TableColumn<>("Datum");
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("p2"));
		dateColumn.setPrefWidth(100);

		presenceColumn = new TableColumn<>("Anwesend");
		presenceColumn.setCellValueFactory(new PropertyValueFactory<>("p3"));
		presenceColumn.setPrefWidth(100);

		table.getColumns().addAll(groupColumn, idColumn, dateColumn, presenceColumn);
		table.setItems(data);

		StackPane secondaryLayout = new StackPane();
		secondaryLayout.getChildren().add(table);

		Scene secondScene = new Scene(secondaryLayout, 550, 600);

		// New window (Stage)
		Stage newWindow = new Stage();
		newWindow.setTitle("Gruppen");
		newWindow.setScene(secondScene);

		newWindow.show();

		if(errorOccurred) {
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.setTitle("Korrupte Daten");
			alert.setHeaderText("Unvollständige or korrupte Daten vom Server empfangen");
			alert.setContentText("Manche Daten vom Server scheinen beschädigt gewesen zu sein.");
			alert.showAndWait();
		}
	}
}

